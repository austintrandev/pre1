package com.hellocamp;

public class HelloDevCamp {

	public static void main(String[] args) {
		//System.out.println("Hello DevCamp!!");
		Person person = new Person();
		
		person.addCar("Volvo");
		person.addCar("BMW");
		person.printAllCars();
		
		person = new Person(10,12,"Tran","Dang");
		System.out.println(person.getFullName());
		
		Student student = new Student("Nguyen");
		System.out.println(student.getFirstName());
		
	}

}
