package com.hellocamp;

public class Person {
	int age;
	int id;
	String firstName;
	String lastName;
	String[] cars = new String[0];
	public Person() {
		
	}
	public Person(int pAge, int pId, String pFirstName, String pLastname) {
		id = pId;
		this.age = pAge;
		firstName = pFirstName;
		this.lastName = pLastname;
	}
	public Person(int pAge, int pId, String pFirstName, String pLastname, String[] cars) {
		id = pId;
		this.age = pAge;
		firstName = pFirstName;
		this lastName = pLastname;
		this.cars = cars;
	}
	public String getFullName() {
		return this.firstName + " " + this.lastName;
	}
	public void addCar(String car) {
		int arrayLength = this.cars.length;
		String[] newCars = new String[arrayLength+1];
		for (int i = 0; i < cars.length; i++) {
			newCars[i] = cars[i];
		}
		newCars[arrayLength] = car;
		this.cars = newCars;
	}
	public void printAllCars() {
		System.out.print("[");
		for (var i = 0; i < cars.length; i++) {
			System.out.print(cars[i]+",");
		}
		System.out.println("]");
	}
}
